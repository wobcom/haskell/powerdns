-- |
-- Module: PowerDNS.API
-- Description: Servant based wrapper for PowerDNS.
--
-- This module exports a complete servant API description of the PowerDNS. May be useful to some.

{-# LANGUAGE DataKinds #-}
{-# LANGUAGE TypeOperators #-}
{-# LANGUAGE DeriveGeneric #-}
module PowerDNS.API
  ( API
  , api
  , PowerDNS(..)
  , module PowerDNS.API.Version
  , module PowerDNS.API.Zones
  , module PowerDNS.API.Servers
  , module PowerDNS.API.Cryptokeys
  , module PowerDNS.API.Metadata
  , module PowerDNS.API.TSIGKeys
  )
where

import           GHC.Generics (Generic)
import           Data.Proxy (Proxy(..))

import           Servant.API
import           Servant.API.Generic ((:-), ToServantApi)

import           PowerDNS.API.Version
import           PowerDNS.API.Zones
import           PowerDNS.API.Servers
import           PowerDNS.API.Cryptokeys
import           PowerDNS.API.Metadata
import           PowerDNS.API.TSIGKeys

api :: Proxy API
api = Proxy

type API = ToServantApi PowerDNS

data PowerDNS f = PowerDNS
  { versions   :: f :- "api" :>         ToServantApi VersionsAPI
  , servers    :: f :- "api" :> "v1" :> ToServantApi ServersAPI
  , zones      :: f :- "api" :> "v1" :> ToServantApi ZonesAPI
  , cryptokeys :: f :- "api" :> "v1" :> ToServantApi CryptokeysAPI
  , metadata   :: f :- "api" :> "v1" :> ToServantApi MetadataAPI
  , tsigkeys   :: f :- "api" :> "v1" :> ToServantApi TSIGKeysAPI
  } deriving Generic

