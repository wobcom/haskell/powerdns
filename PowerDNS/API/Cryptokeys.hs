-- |
-- Module: PowerDNS.API.Cryptokeys
-- Description: Cryptokeys endpoints for PowerDNS API
--
-- This module implements the endpoints described at [Cryptokeys API](https://doc.powerdns.com/authoritative/http-api/cryptokey.html)

{-# LANGUAGE DataKinds          #-}
{-# LANGUAGE DeriveAnyClass     #-}
{-# LANGUAGE DeriveDataTypeable #-}
{-# LANGUAGE DeriveGeneric      #-}
{-# LANGUAGE TypeOperators      #-}
module PowerDNS.API.Cryptokeys
  (
  -- * API
    CryptokeysAPI(..)

  -- * Data types
  , Cryptokey(..)
  )
where

import           Data.Data (Data)

import           Control.DeepSeq (NFData)
import           Data.Aeson (FromJSON(..), ToJSON(..), defaultOptions,
                             fieldLabelModifier, genericParseJSON,
                             genericToJSON)
import qualified Data.Text as T
import           Servant.API
import           Servant.API.Generic

import           PowerDNS.Internal.Utils (Empty(..), strip)

data CryptokeysAPI f = CryptokeysAPI
    { apiListCryptokeys :: f  :- "servers" :> Capture "server_id" T.Text :> "zones" :> Capture "zone_id" T.Text :> "cryptokeys"
                              :> Get '[JSON] [Cryptokey]

    , apiCreateCryptokey :: f :- "servers" :> Capture "server_id" T.Text :> "zones" :> Capture "zone_id" T.Text :> "cryptokeys"
                              :> ReqBody '[JSON] Cryptokey
                              :> PostCreated '[JSON] Cryptokey

    , apiGetCryptokey :: f    :- "servers" :> Capture "server_id" T.Text :> "zones" :> Capture "zone_id" T.Text :> "cryptokeys" :> Capture "cryptokey_id" T.Text
                              :> Get '[JSON] Cryptokey

    , apiUpdateCryptokey :: f :- "servers" :> Capture "server_id" T.Text :> "zones" :> Capture "zone_id" T.Text :> "cryptokeys" :> Capture "cryptokey_id" T.Text
                              :> ReqBody '[JSON] Cryptokey
                              :> PutNoContent

    , apiDeleteCryptokey :: f :- "servers" :> Capture "server_id" T.Text :> "zones" :> Capture "zone_id" T.Text :> "cryptokeys" :> Capture "cryptokey_id" T.Text
                              :> DeleteNoContent

    } deriving Generic

----------------------------------------------------------------------------------------

data Cryptokey = Cryptokey
    { ck_type :: Maybe T.Text
    , ck_id :: Maybe Integer
    , ck_keytype :: Maybe T.Text
    , ck_active :: Maybe Bool
    , ck_published :: Maybe Bool
    , ck_dnskey :: Maybe T.Text
    , ck_ds :: Maybe [T.Text]
    , ck_privatekey :: Maybe T.Text
    , ck_algorithm :: Maybe T.Text
    , ck_bits :: Maybe Integer
    } deriving (Eq, Ord, Show, Generic, NFData, Data, Empty)

instance ToJSON Cryptokey where
  toJSON = genericToJSON defaultOptions { fieldLabelModifier = strip "ck_"}

instance FromJSON Cryptokey where
  parseJSON = genericParseJSON defaultOptions { fieldLabelModifier = strip "ck_"}
