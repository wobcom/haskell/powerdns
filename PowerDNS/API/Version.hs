-- |
-- Module: PowerDNS.API.Zones
-- Description: API version endpoint for PowerDNS API
--
-- Datatype for version API endpoint

{-# LANGUAGE DataKinds          #-}
{-# LANGUAGE DeriveAnyClass     #-}
{-# LANGUAGE DeriveDataTypeable #-}
{-# LANGUAGE DeriveGeneric      #-}
{-# LANGUAGE TypeOperators      #-}
module PowerDNS.API.Version
  (
  -- * API
    VersionsAPI(..)

  -- * Data types
  , Version(..)
  )
where

import           Data.Data (Data)

import           Control.DeepSeq (NFData)
import           Data.Aeson (FromJSON(..), ToJSON(..), defaultOptions, fieldLabelModifier,
                             genericParseJSON, genericToJSON)

import qualified Data.Text as T
import           Servant.API
import           Servant.API.Generic

import           PowerDNS.Internal.Utils (strip)

data VersionsAPI f = VersionsAPI
  { apiListVersions :: f :- Get '[JSON] [Version]
  } deriving Generic

data Version = Version
  { version_version :: Int
  , version_url :: T.Text
  } deriving (Eq, Ord, Show, Generic, NFData, Data)

instance ToJSON Version where
  toJSON = genericToJSON defaultOptions { fieldLabelModifier = strip "version_" }

instance FromJSON Version where
  parseJSON = genericParseJSON defaultOptions { fieldLabelModifier = strip "version_" }
  
