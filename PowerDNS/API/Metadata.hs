-- |
-- Module: PowerDNS.API.Metadata
-- Description: Metadata endpoints for PowerDNS API
--
-- This module implements the endpoints described at [Metadata API](https://doc.powerdns.com/authoritative/http-api/metadata.html#)

{-# LANGUAGE DataKinds          #-}
{-# LANGUAGE DeriveAnyClass     #-}
{-# LANGUAGE DeriveDataTypeable #-}
{-# LANGUAGE DeriveGeneric      #-}
{-# LANGUAGE TypeOperators      #-}
module PowerDNS.API.Metadata
  (
  -- * API
    MetadataAPI(..)

  -- * Data types
  , Metadata(..)
  )
where

import           Control.DeepSeq (NFData)
import           Data.Aeson ( FromJSON(..), ToJSON(..)
                            , defaultOptions, fieldLabelModifier
                            , genericParseJSON, genericToJSON
                            )

import           Data.Data (Data)
import qualified Data.Text as T
import           Servant.API
import           Servant.API.Generic

import           PowerDNS.Internal.Utils (strip)

data MetadataAPI f = MetadataAPI
  { apiListMetadata   :: f :- "servers" :> Capture "server_id" T.Text :> "zones" :> Capture "zone_id" T.Text :> "metadata"
                           :> Get '[JSON] [Metadata]

  , apiCreateMetadata :: f :- "servers" :> Capture "server_id" T.Text :> "zones" :> Capture "zone_id" T.Text :> "metadata"
                           :> ReqBody '[JSON] Metadata
                           :> PostNoContent

  , apiGetMetadata    :: f :- "servers" :> Capture "server_id" T.Text :> "zones" :> Capture "zone_id" T.Text :> "metadata" :> Capture "kind" T.Text
                           :> Get '[JSON] Metadata

  , apiUpdateMetadata    :: f :- "servers" :> Capture "server_id" T.Text :> "zones" :> Capture "zone_id" T.Text :> "metadata" :> Capture "kind" T.Text
                           :> ReqBody '[JSON] Metadata
                           :> Put '[JSON] Metadata

  , apiDeleteMetadata    :: f :- "servers" :> Capture "server_id" T.Text :> "zones" :> Capture "zone_id" T.Text :> "metadata" :> Capture "kind" T.Text
                           :> DeleteNoContent
  } deriving Generic

data Metadata = Metadata
  { md_kind :: T.Text
  , md_metadata :: [T.Text]
  } deriving (Eq, Ord, Show, Generic, NFData, Data)

instance ToJSON Metadata where
  toJSON = genericToJSON defaultOptions { fieldLabelModifier = strip "md_"}

instance FromJSON Metadata where
  parseJSON = genericParseJSON defaultOptions { fieldLabelModifier = strip "md_"}
