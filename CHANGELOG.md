# Revision history for powerdns-api

## 0.1 -- 2021-05-28

* Initial release

## 0.1.1 -- 2021-06-08

* Relax base constraint

## 0.2 -- 2021-11-16

* Add missing FromHttpApiData instance for ObjectType
* Fix incorrect ToHttpApiData instance for ObjectType
* Rename incorrectly named data constructor for CryptokeysAPI
* Add versions endpoint

## 0.2.1 -- 2022-01-12
* Correctly parse and serialize ObjectType, Kind and ChangeType

## 0.2.2 -- 2022-01-27
* Adjust `Eq` on `Zone` and `RRSet` to compare up to limited case-sensitity

## 0.3 -- 2022-03-02

* Rename typod field name `commant_modified_at` to `comment_modified_at`
* Wrap `rrset_name` and `zone_name` with new `CIText` wrappers for limited case-sensitivity
* Have `Ord` instances of `Zone` and `RRSet` correctly respect limited case-sensitivity

## 0.4.0 -- 2022-03-15

* Wrap `rrset_ttl` with Maybe to correctly allow deleting records

## 0.4.1 -- 2022-04-22

* Relax various constraints
