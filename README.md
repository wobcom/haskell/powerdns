# PowerDNS API

## Tests

The tests currently assume a PowerDNS to be running locally, with the webserver at http://localhost:8081 and the api key set to 'secret'

One can use docker to build a local PowerDNS quickly.
```sh
git clone https://github.com/PowerDNS/pdns/

cd pdns

git checkout rel/auth-4.4.x
git submodule init
git submodule update

docker build -f Dockerfile-auth . -t pdns-auth-44

docker run --rm -p8081:8081 pdns-auth-44:latest -- --api --api-key=secret --webserver-address=0.0.0.0 --webserver-allow-from=172.0.0.0/8,::1
```
